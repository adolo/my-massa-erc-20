use std::collections::HashMap;

use primitive_types::U256;

//use massa-tools;

/// syscalls that must be avaible in the Smart contract engine
trait MassaTools {
    fn massa_transfer(&mut self, emitter: Address, receiver: Address, amount: u64) -> bool;
    fn get_balance(&self, address: Address) -> u64;
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Address;

trait Erc20 {
    /// Returns the total token supply.
    fn total_supply(&self) -> U256;

    /// Returns the account balance of another account with address_owner.
    fn balance_of(&self, address_owner: Address) -> U256;

    /// Transfers _value amount of tokens to address _to,
    /// and MUST fire the Transfer event.
    /// The function SHOULD throw
    /// if the message caller’s account balance does not have enough tokens to spend.
    /// Note Transfers of 0 values MUST be treated as normal transfers and fire the Transfer event.
    fn transfer(&mut self, to: Address, value: U256) -> bool;

    /// Transfers _value amount of tokens from address _from to address _to,
    /// and MUST fire the Transfer event.
    /// The transferFrom method is used for a withdraw workflow,
    /// allowing contracts to transfer tokens on your behalf.
    /// This can be used for example to allow a contract to transfer tokens
    /// on your behalf and/or to charge fees in sub-currencies.
    /// The function SHOULD throw unless the _from account has deliberately authorized
    /// the sender of the message via some mechanism.
    /// Note Transfers of 0 values MUST be treated as normal transfers and fire the Transfer event.
    fn transfer_from(&mut self, from: Address, to: Address, value: U256) -> bool;

    /// Allows _spender to withdraw from your account multiple times,
    /// up to the _value amount.
    /// If this function is called again it overwrites the current allowance with _value.
    /// NOTE: To prevent attack vectors like the one described here
    /// and discussed here, clients SHOULD make sure to create user interfaces
    /// in such a way that they set the allowance first to 0
    /// before setting it to another value for the same spender.
    /// THOUGH The contract itself shouldn’t enforce it,
    /// to allow backwards compatibility with contracts deployed before
    fn approuve(&mut self, spender: Address, value: U256) -> bool;

    /// Returns the amount which _spender is still allowed to withdraw from _owner.
    fn allowance(&self, owner: Address, spender: Address) -> U256;
}

struct MyCoin {
    current_supply: U256,
    balances: HashMap<Address, U256>,
    caller: Address,
    allowances: HashMap<Address, HashMap<Address, U256>>, // List of addresses the adress is autorized to use
    sc_massa_address: Address,
}

impl Erc20 for MyCoin {
    fn total_supply(&self) -> U256 {
        self.balances
            .iter()
            .fold(U256::zero(), |acc, (_, a)| acc + *a)
            + self.current_supply
    }

    fn balance_of(&self, address_owner: Address) -> U256 {
        self.balances
            .get(&address_owner)
            .copied()
            .unwrap_or_default()
    }

    fn transfer(&mut self, to: Address, value: U256) -> bool {
        if self.balance_of(self.caller) >= value {
            self.balances
                .entry(self.caller)
                .and_modify(|old| *old = *old - value);
            self.balances
                .entry(to)
                .and_modify(|old| *old = *old + value)
                .or_insert(value);
            true
        } else {
            false
        }
    }

    fn transfer_from(&mut self, from: Address, to: Address, value: U256) -> bool {
        if self
            .allowances
            .get(&self.caller)
            .unwrap_or(&HashMap::new())
            .get(&from)
            .unwrap_or(&U256::zero())
            > &value
        {
            if self.balance_of(from) >= value {
                self.balances
                    .entry(from)
                    .and_modify(|old| *old = *old - value);
                self.balances
                    .entry(to)
                    .and_modify(|old| *old = *old + value)
                    .or_insert(value);
                self.allowances
                    .get_mut(&self.caller)
                    .unwrap_or(&mut HashMap::new())
                    .entry(from)
                    .and_modify(|old| *old = *old - value);

                true
            } else {
                false
            }
        } else {
            false
        }
    }

    fn approuve(&mut self, spender: Address, value: U256) -> bool {
        self.allowances
            .entry(self.caller)
            .and_modify(|old| {
                old.entry(spender)
                    .and_modify(|old| *old = value)
                    .or_insert(value);
            })
            .or_insert_with(|| {
                let mut h = HashMap::new();
                h.insert(spender, value);
                h
            });
        true
    }

    fn allowance(&self, owner: Address, spender: Address) -> U256 {
        *self
            .allowances
            .get(&spender)
            .unwrap_or(&HashMap::new())
            .get(&owner)
            .unwrap_or(&U256::zero())
    }
}

impl MyCoin {
    fn coin_request(&mut self, amount: u64, tools: &mut impl MassaTools) -> bool {
        if tools.get_balance(self.caller) >= amount {
            if tools.massa_transfer(self.caller, self.sc_massa_address, amount) {
                self.current_supply = self.current_supply - amount;
                self.balances
                    .entry(self.caller)
                    .and_modify(|old| *old = *old + amount)
                    .or_insert(amount.into());
                true
            } else {
                false
            }
        } else {
            false
        }
    }

    fn coin_sell(&mut self, amount: u64, tools: &mut impl MassaTools) -> bool {
        if self.balance_of(self.caller) >= amount.into() {
            self.current_supply = self.current_supply + amount;
            self.balances
                .entry(self.caller)
                .and_modify(|old| *old = *old - amount)
                .or_insert(amount.into());
            tools.massa_transfer(self.sc_massa_address, self.caller, amount);
            true
        } else {
            false
        }
    }
}

fn main() {
    println!("Hello, world!");
}
